<?php
//22.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd kiírja, hogy a tömbben hány darab olyan szám van, amely nagyobb, mint 10 és kisebb 20.
$szamTomb = [];
$db=0; //darabszám ez legyen kezdetben 0
while(count($szamTomb)<10){
    $szam = rand(1,30);
    if($szam > 10 AND $szam < 20){//ha 10 és 20 közötti értéket teszünk a tömbbe, léptetjük 1 el a db számot
        $db++;
    }
    $szamTomb[]=$szam;
}
echo '<pre>'.var_export($szamTomb,true).'</pre>';
echo "A 10 és 20 közötti értékek száma: $db db";

//24.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, amelyek valamilyen termékek nettó árai, majd számolja ki és írassa ki a termékek bruttó árait, ha az ÁFA 20%
//1. tömb felépítése (500 - 5000)
//$arak = [500,570,980,1120,...];
$arak = [];
while(count($arak)<10){
    $arak[]=rand(500,5000);;
}
echo '<pre>'.var_export($arak,true).'</pre>';
//bejárás(foreach) és értékek számítása/kiírása
//A termék ára: $value + 20% Áfa =  brutto:
foreach($arak as $value){
    $brutto = $value*1.2;//brutto számítása az aktuális értékre
    echo "<br>A termék ára: $value + 20% Áfa =  brutto: $brutto";
}
//@todo: hf: hazi-feladatok.txt - összes
//@todo: feladatgyüjtemény: +6,7
//@todo: dobj egy kockával 6szor és mond meg a dobások összegét
