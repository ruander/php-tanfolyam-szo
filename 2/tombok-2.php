<?php
$napok = [
    'Hétfő','Kedd','Szerda','Csütörtök','Péntek','Szombat','Vasárnap'
];
echo '<pre>'.var_export($napok,true).'</pre>';
//aktuális nap száma
$nap = date('N');
echo $nap-1;
//írjuk ki milyen nap van ma
echo "<h3>Ma {$napok[$nap-1]} van</h3>";
//tömb elemszáma
echo 'A napok tömb elemszáma:'.count($napok);
//írjuk ki a napokat
for($i=0;$i<count($napok);$i++){
    echo "<br>$napok[$i]";
}
//tömb bejárása
/**
 foreach($tomb as [$key =>] $value){
 //ciklusmag
 }
 */
foreach($napok as $key => $value){
    echo "<br>Az aktuális kulcs: $key, érték: $value";
}
//Több dimenziós tömb
$tomb = [
  1 => 'Első elem',
  23 => pi(),
  'status' => true,
  'user' => [
      'id' => 1,
      'email' => 'hgy@iworkshop.hu',
      'password' => '123456'
  ],
  'date' => date('y-m-d H:i:s')
];
echo '<pre>'.var_export($tomb,true).'</pre>';
//változó tipusának vizsgálata
echo '<br>'.gettype($tomb);
echo '<br>'.gettype($tomb[1]);
echo '<br>'.gettype($tomb[23]);

//összes tipus a tömb 1 dimenzióján: A kulcs: 1, az érték tipusa: string
foreach($tomb as $key => $value){
    echo "<br>A kulcs: $key, az érték tipusa: ".gettype($value);
}

//kiírás nem 1. dimenzióról
echo '<br>'.$tomb['user']['email'];