<?php
/**
 hátultesztelő változat
 * do{
 //ciklusmag
 }while(condition);
 * @todo: hf utánanézni
 */
/*
 elöltesztelő változat:
while(belépési feltétel){
    //ciklusmag
}
 */
//for működés szim.
/*
 for($i=1;$i<=10;$i++){echo $i;}
 */
$i=1;//'ciklusváltozó kezdeti értéke'
while($i<=10){//belépési feltétel vizsgálat
    echo "<br>$i";
    //ciklusváltozó léptetése
    $i++;
}

//while használata pl egy tömb feltöltés közben
//készítsünk egy tömböt   páros számmal 1-100 közötti értékekkel
//elemi rész: generáljunk 1-100 közötti számot
//tároljuk el egy tömb következő elemeként -> ha páros
//csináljuk ezt 20x
$szamTomb=[];//ide gyűjtjük a számokat
for($i=1;$i<=20;$i++){
    $szam = rand(1,100);
    if($szam%2==0){
        $szamTomb[]=$szam;
    }
}
echo '<pre>'.var_export($szamTomb,true).'</pre>';
//^^^^ ez igy nem jo mert nem lesz 20 eleme a tömbbnek

//while és tömb elemszám segítségével
$szamTomb = [];
while(count($szamTomb)<20){
    $szam = rand(1,100);
    if($szam%2==0){
        $szamTomb[]=$szam;
    }
}
echo '<pre>'.var_export($szamTomb,true).'</pre>';
//ugyanez 'hiányos' de jól működő forral
$szamTomb=[];//ide gyűjtjük a számokat
for(;count($szamTomb)<20;){
    $szam = rand(1,100);
    if($szam%2==0){
        $szamTomb[]=$szam;
    }
}
echo '<pre>'.var_export($szamTomb,true).'</pre>';