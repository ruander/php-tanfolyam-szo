<?php
//saját eljárások gyüjteménye
/**
 * Saját hiba kiíró eljárás
 * a $hiba változóban tárolt hibák kiírására
 * @param $inputName
 * @return bool
 */
function hibaKiir($inputName)
{
    global $hiba;//az eljárás idejére elérhetővé tesszük a hibatömböt

    if (isset($hiba[$inputName])) {
        return $hiba[$inputName];
    }
    return false;
}
/**
 * @param $fieldName
 * @param $rowData
 * @param string $type
 * @return mixed|string
 */

function checkMyInput($fieldName, $rowData, $type = 'text')
{
    switch ($type) {
        case 'checkbox':
            return checkBoxTest($fieldName, $rowData);
        default:
            return checkValue($fieldName, $rowData);
    }
}

/*Értékek visszaadása az űrlapok elemeihez*/
/**
 * @param $fieldName :string
 * @param $rowData :string
 * @return mixed|string
 */
function checkValue($fieldName, $rowData)
{
    $fieldData = filter_input(INPUT_POST, $fieldName);
    if ($fieldData !== null) {
        return $fieldData;//ha van post adat azt adjuk vissza ha nincs,
    } elseif ($rowData != '') { //de van db adat akkor azt adjuk vissza
        return $rowData;
    }
    return '';//sehol nincs semmi, azt adjuk vissza
}

//ez most a checkboxokhoz lesz:
function checkBoxTest($fieldName, $rowData)
{
    $fieldData = filter_input(INPUT_POST, $fieldName);
    var_dump($fieldName,$rowData);
    if (empty($_POST) && $rowData == 0 || !empty($_POST) && $fieldData === NULL) { //ha üres a post és row 0 akkor nem kell checked || van post de nincs kipipálva tehát nincs a postban az adott elem
        return '';
    }

    return 'checked';
}