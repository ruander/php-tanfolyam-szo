<?php
//pure php file
echo 'Hello PHP!';
//6 elemű lista létrehozása ciklus segítségével
/*
for(ciklusváltozó kezdeti értéke;belépési feltétel vizsgálata; ciklusváltozó lépteése){
    //ismétlődő programrész - ciklusváltozó elérhető
}
 */
echo '<ul>';//lista nyitás
for( $i = 1 ; $i <= 6 ; $i++){// operátor: ++ -> $i = $i + 1
    echo "<li>listaelem $i</li>";//listaelemek
}
echo '</ul>';//lista zárás
//csináljunk ugyanolyan listát, de egy döntés alapján adjunk háttérszínt neki
/*
 * Generáljunk egy számot  (dobjunk egy 6 oldalú kockával)
 * ezt írjuk ki
 * ha az eredmény páros, legyen halványszürke a lista,
 * ha páratlan legyen halványkék
 */
$generaltSzam = rand(1,6);
echo "<h3>A generált érték: $generaltSzam</h3>";
//elágagazás a PHP ban
/*
if(feltétel){
    igaz ág
}else{
    hamis ág
}
 */
if($generaltSzam%2 == 0 ){//operátor: % -> maradékos osztás; == -> értékékegyezés vizsgálat; === -> érték és tipus!!!!! egyezés vizsgálat
    //páros
    $color = 'lightgray';
}else{
    //páratlan
    $color = 'lightblue';
}
/*
echo '<pre>';
var_dump($generaltSzam,$color);//változó információk kiírása (fejlesztés közben!!!)
echo '</pre>';
*/
//a ciklus de most egy echo -val, segédváltozóval
$lista = '<ul style="background: '.$color.';">';//lista nyitás változóba
for( $i = 1 ; $i <= 6 ; $i++){// operátor: ++ -> $i = $i + 1
    $lista .= "<li>listaelem $i</li>";//listaelemek stringbe fűzése
}
$lista .= '</ul>';//lista zárás a listaelemek után a változóba //<ul><li></li>...</ul> | $lista .= '</ul>' -> $lista = $lista . '</ul>'; az analógia működik : +=, -= , *= , /= , .=
echo $lista;//kiírás egy lépésben


$year = date('Y');
$footer = '<footer>PHP tanfolyam - Ruander oktatóközpont | 2010 - '.$year.' &copy; Minden jog fenntartva!</footer>';
//kiírás
echo $footer;