<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Házi feladatok megoldásai</title>
</head>
<body>
<h2>1. Írjon egy php programot, amely kiszámolja és kiírja a 2&nbsp;m élhosszúságú kocka felületét.</h2>
<div class="megoldas">
    <?php
    $el = 2;
    $felulet = $el * $el * 6;
    //megoldás kiírása
    echo "Egy $el&nbsp;m élhosszúságú kocka felülete $felulet&nbsp;m<sup>2</sup>";

    ?>
</div>
<h2>2.Írjon egy php programot, amely kiszámolja és kiírja a 2&nbsp;m sugarú gömb térfogatát. A gömb térfogata V=4/3
    r^3pi.</h2>
<div class="megoldas">
    <?php
    $sugar = 2;
    $terfogat = pow($sugar, 3) * pi() * 4 / 3;
    //megoldás kiírása
    echo "Egy $el&nbsp;m sugarú gömb térfogata $terfogat&nbsp;m<sup>3</sup>";
    ?>
</div>
<h2>3.Írjon egy php programot, amely kiszámolja és kiírja egy derékszögű háromszög átfogójának hosszát, ha a befogói 2&nbsp;m
    és 5&nbsp;m hosszúak.</h2>
<div class="megoldas">
    <?php
    $a = 2;
    $b = 5;
    $c = sqrt(pow($a, 2) + pow($b, 2));//sqrt -> négyzetgyök, pow(alap,kitevő) -> hatványozás
    //megoldás kiírása
    echo "Egy $a&nbsp;m és $b&nbsp;m befogojú 3szög átfogója $c&nbsp;m";
    ?>
</div>
<h2>12.Írjon egy php programot, amely előállít egy XxY cellát tartalmazó táblázatot és mindegyik cellában a Google szót helyezi el a cellák jobb felső sarkába.</h2>
<div class="megoldas">
    <?php
    //stílus megadása változóba
    $style = '<style>
                table.feladat-12 td {
                    width:200px;
                    height: 50px;
                    vertical-align: top;
                    text-align: right;
                }
              </style>';
    echo $style;
    $megoldas = '<table border="1" class="feladat-12">';//table nyitó tag
    $megoldas .= '<tr>';//$megoldas = $megoldas . '<tr>'; működik az analógia +=, -= *= /=
    $megoldas .= '<td>Google</td>';//1 cella, most 3 soros 2 oszlopos táblázatot készítünk
    $megoldas .= '<td>Google</td>';//1 cella, most 3 soros 2 oszlopos táblázatot készítünk
    $megoldas .= '</tr>';
    //egy teljes sor fűzése
    $megoldas .= '<tr><td>Google</td><td>Google</td></tr>';
    $megoldas .= '<tr><td>Google</td><td>Google</td></tr>';
    $megoldas .= '</table>';//table zárása
    //megoldás kiírása változóból
    echo $megoldas;
    ?>
</div>
<h2>16.Írjon egy programot, amely kiszámolja az 1*3*5*7*9*11*13*15*17*19 értéket ciklus utasítás segítségével.</h2>
<div class="megoldas">
    <?php
//megoldás 1
    $mtpl = 1;//ide gyűjtjük a szorzatot
   for($i=1;$i<=19;$i++){
       //kiírjuk a páratlan számokat
       if($i%2 == 1) {
           $mtpl *= $i;//az addigi változóértéket megszorozzuk az aktuális ciklusváltozóval
       }
   }
   echo "A szorzat: $mtpl";
   //picit másképp
    $mtpl=1;
    for($i=1;$i<=19;$i+=2){
        $mtpl *= $i;
    }
    echo "<br>A szorzat: $mtpl";
    ?>
</div>
<h2>19.Írjon egy programot ciklus utasítást használva, amely az alábbi elrendezésű szöveget írja ki a képernyőre:
<br>2
<br>22
<br>222
<br>2222
</h2>
<div class="megoldas">
    <?php
//1. megoldás string hozzáfűzés, de ez nem segít a 20. feladat megoldásában
    $kiir = '<br>';
    //ciklus a 2eseknek
    for($i=1;$i<=4;$i++){
        echo $kiir.='2';
    }
    //2. megoldás egymásba ágyazott ciklusokkal
    //külső ciklus a soroknak
    for($i=1;$i<=4;$i++){
        echo '<br>';
        //beágyazott ciklus, elfut 1 től a külső ciklusváltozóig és kiír 2eseket
        for($j=1;$j<=$i;$j++){
            echo '2';
        }
    }
//20. feladat megoldása a 19. visszafele
    //külső ciklus a soroknak
    for($i=4;$i>=1;$i--){
        echo '<br>';
        //beágyazott ciklus, elfut 1 től a külső ciklusváltozóig és kiír 2eseket
        for($j=1;$j<=$i;$j++){
            echo '2';
        }
    }
//19es rövidebben
    for($i=1;$i<=4;$i++) {
        echo '<br>'.str_repeat('2', $i);
    }
    //20as rövidebben
    for($i=4;$i>=1;$i--){
        echo '<br>'.str_repeat('2', $i);
    }
    ?>
</div>
</body>
</html>
