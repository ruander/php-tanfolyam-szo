<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Feladatgyűjtemény pdf megoldásai</title>
    <style>
        .kiemelt {
            background: burlywood;
            color: darkblue;
        }
    </style>
</head>
<body>

<h2>1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.</h2>
<div class="megoldas">
<?php
//@todo: HF kidolgozni 1-5 ami nincs kész mind

?>
</div>
<h2>2. Készítsünk programot, amely kiszámolja az első 100 darab. természetes szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a program elején kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)</h2>
<div class="megoldas">
<?php
$sum = 0;
for($i=1;$i<=100;$i++){
    $sum += $i;
}
echo "A számok összege 1 - 100 ig: $sum";
?>
</div>
<h2>3. Készítsünk programot, amely kiszámolja az első 7 darab. természetes szám szorzatát egy ciklus segítségével. (A szorzat kiszámolásához vezessünk be egy változót, amelyet a program elején beállítunk 1-re, a ciklusmagban pedig mindig hozzászorozzuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, ..., 7 számokat.)</h2>
<div class="megoldas">
<?php
$mtp = 1;//itt alakítjuk ki a szorzatot
for($i=1;$i<=7;$i++){
    $mtp *= $i;
}
echo "A számok szorzata 1 - 7 ig: $mtp";
//$i értéke itt: 8 !!! ezért nem lépett be a ciklusba a kód még 1x
//echo $i;
?>
</div>
<h2>6. Készítsen egy olyan ciklust, ami egy fejléces táblázatot ír ki.</h2>
<div class="megoldas">
<?php
//gyors megoldás fix oszlopokkal
$table = '<table border="1">';//table nyitás

$rows = 14;//ennyi sort szeretnénk
$cols = 6;//ennyi oszlopot szeretnénk
$kiemeltSor = 2;//ezt szeretnénk kiemelni (most minden 5. sort)
$kiemeltOszlop = 4;//ezt az oszlopot látjuk el kiemelt classal
$kizartSorElemek=[];//kizárt sorok sorszámai
//sorok
for($sor=1;$sor<=$rows;$sor++){

    if(//kiemelt sor kell legyen?
            $kiemeltSor !== false //kell kiemelés
            AND //és
            ( $sor == 1 //első sorba mindenképp
            OR //vagy
                //nem az első sorban vagyunk
                $sor%$kiemeltSor == 0 //kell kiemelés
            AND //és
                $kiemeltSor > 1 //kell több kiemelés
                )
            AND !in_array($sor,$kizartSorElemek)
    ){
            $table .= '<tr class="kiemelt">';
        }else{
            $table .= '<tr>';
    }
    //oszlopok / belső (beágyazott) ciklus
        for($oszlop=1;$oszlop<=$cols;$oszlop++){
            //kiemelt oszlop-e? === !== == !=
            if(
                $kiemeltOszlop !== false //kell kiemelés
                AND //és
                ( $oszlop == 1 //első oszlopba mindenképp
                    OR //vagy
                    //nem az első oszlopban vagyunk
                    $oszlop%$kiemeltOszlop == 0 //kell kiemelés
                    AND //és
                    $kiemeltOszlop > 1 //kell több kiemelés
                )    
            ){
                $table .= "<td class='kiemelt'>cella: $sor | $oszlop</td>";
            }else{
                $table .= "<td>cella: $sor | $oszlop</td>";
            }
        }
    $table .='</tr>';
}
$table .= '</table>';//táblázat zárása

echo $table;//kiírás egy lépésben
?>
</div>
<h2>7. Készítsen egy olyan ciklust, amely egy kiemelt első oszlopot tartalmazó táblázatot rajzol ki.</h2>
<div class="megoldas">
<?php
//gyors megoldás fix oszlopokkal
$table = '<table border="1">';//table nyitás
//sorok
for($sor=1;$sor<=$rows;$sor++){
    $table .= '<tr>
                <td class="kiemelt">'.$sor.'</td>
                <td>name</td>
                <td>email</td>
              </tr>';
}
$table .= '</table>';//táblázat zárása

echo $table;//kiírás egy lépésben
?>
</div>
</body>
</html>
