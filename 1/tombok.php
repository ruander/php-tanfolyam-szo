<?php
//a tömbök halmazok, az értékeik indexen (vagy kulcson' találhatóak
$tomb = array( 'valami', 12, 7/6, true );//tömb fmegadása értékekkel, automatikus indexre
var_dump('<pre>', $tomb);
////////////////////////////////////
$tomb = [];//üres tömb létrehozása, vagy meglévő ürítése
$tomb = ['helo world', 2020 ];
$tomb[] = 'Május';//tomb elemeinek bővítése automatikus indexre
$tomb[100] = 'Horváth György';
$tomb[] = 'stuff';//tomb elemeinek bővítése automatikus indexre
var_dump($tomb);
$tomb['email'] = 'hgy@iworkshop.hu';//tomb elemeinek bővítése asszociatív indexre
var_dump($tomb);
//tömb elemeinek használata
echo $tomb[100];//elem elérése indexről
//kiegészítve:
echo "<div>Név: $tomb[100] - [{$tomb['email']}]</div>";//asszociatv index esetén a "" között kell egy {} határoló hogy az érték jelenjen meg

unset($tomb[2]);//elem eltávolítása indexel együtt
var_dump($tomb);