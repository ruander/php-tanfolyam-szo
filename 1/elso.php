<!doctype html>
<html lang="hu">
 <head>
	 <title>Első php fileom</title>
	 <meta charset="utf-8">
 </head>
 <body>
<?php
//egysoros komment
/*
több 
soros
komment
*/
	echo "Hello World!";//echo kiír egy szöveget a standard outputra - azaz alapértelmezetten a fileba ahol fut | ; -> minden utasítást ; -vel zárunk
	print "<br>Hello again!";//kb ua mint az echo, csak van returnje | operátor "" -> szöveggé konvertálja a benne foglalt elemeket
	echo '<ul style="color:green;">
			<li>listaelem 1</li>
			<li>listaelem 1</li>
			<li>listaelem 2</li>
			<li>listaelem 3</li>
	      </ul>';//komplexebb html kiírása
	
//változók - primitívek
$szoveg = 'ez egy szöveg';// tipus: string - operátor: $ -> változó, = -> értékadó operátor
$egesz_szam = 134;//tipus: int vagy integer; valami_nev_elvalasztva -> snake case
$lebegoPontos = 23/6;// tipus: float vagy floating point number	| valamiNevElvalasztva -> camel case
$logikai = true;// tipus: bool vagy boolean, kiírva : true:1, false:semmi	  
//műveletek változókkal
echo '<div>';//tag nyitás kiírása
echo 'Az $egesz_szam és a $lebegoPontos szám összege: ';//tag tartalom kiírása
echo $egesz_szam + $lebegoPontos;//operátor: +,-,*,/ sima matematikai műveletek
echo '</div>';//tag zárás kiírása		  
// Az "" értelmezi a primitíveket és befordul az 'értékére' az elem
echo '<div>';//tag nyitás kiírása
echo "Az $egesz_szam és a $lebegoPontos szám összege: ";//tag tartalom kiírása
echo $egesz_szam + $lebegoPontos;//operátor: +,-,*,/ sima matematikai műveletek
echo '</div>';//tag zárás kiírása		  
//stringek összefűzése
echo '<div>Az $egesz_szam és a $lebegoPontos szám összege: ' . ($egesz_szam + $lebegoPontos) . '</div>';// operátor: . -> konkatenáció mindkét oldalát stringgé konvertálja
//az összeget tároljuk el egy változóba:
$osszeg = $egesz_szam + $lebegoPontos;
echo "<div>Az $egesz_szam és a $lebegoPontos szám összege: $osszeg</div>";
echo "<div>Az \$egesz_szam és a \$lebegoPontos szám összege: $osszeg</div>";//operátor: \ -> escape, kilépteti a nyelvi elemek közül az utána következő karaktert 
echo $logikai;//true esetén 1, egyébként üres string
//véletlen szám generálása PHP ban
$veletlenSzam = rand();//0 és getrandmax() közé generál
//echo '<br> getrandmax a rendszerünkben: '. getrandmax();
echo "<br>A generált szám: $veletlenSzam";
//generáljunk 1 és 20 közé
$veletlenSzam = rand(1,20);//változó override, redeclare (felülírás vagy újradeklarálás
echo "<br>A generált szám(1,20): $veletlenSzam";
//PHP - Personal Home Page Tools volt eredetileg
//PHP -> PHP: Hypertext Preprocessor -> rekurzív lett az elnevezése :D



?>
 </body>
</html>