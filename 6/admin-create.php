<?php
//erőforrások
include_once("connect.php");

if (!empty($_POST)) {
    $hiba = [];//hibák tömbje
    //nev - nem lehet üres
    $name = trim(filter_input(INPUT_POST, 'name'));
    //szövegvégi spacek eltávolításával
    //$name = trim($name);//spacek eltávolítása a szoveg elejéről és végéről
    if ($name == '') {
        $hiba['name'] = '<span class="error">Kötelező kitölteni!</span>';
    }
    //email - legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Érvénytelen adat!</span>';
    }else{
        //foglalt-e már a db-ben
        $qry = "SELECT id FROM admins WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if(!empty($row)){
            $hiba['email'] = '<span class="error">Már regisztrált email cím!</span>';
        }
    }
    //jelszó min 6 karakter kell legyen
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    if (mb_strlen($pass, "utf-8") < 6) {
        $hiba['pass'] = '<span class="error">Érvénytelen adat (min 6 karakter) !</span>';
    } elseif ($pass != $repass) { //jelszó újra meg kell egyezzen a jelszóval
        $hiba['repass'] = '<span class="error">Érvénytelen adat (a jelszavak nem egyeztek) !</span>';
    } else {
        //$secret_key = 'S3cR3t_K3y!';
        $pass = password_hash($pass, PASSWORD_BCRYPT);
        //password_hash visszafejtése: password_verify(jelszo,tárolt hash)
    }
    //státusz
    $status = filter_input(INPUT_POST,'status')?:0;

    if (empty($hiba)) {
        //adatok 'tisztázása'
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $pass,
            'status' => $status,
            'registrationDate' => date('Y-m-d H:i:s')
        ];
        //new admin create process
        /*
         * INSERT INTO `admins` (`id`, `username`, `email`, `password`, `status`, `lastlogin`, `time_created`, `time_updated`) VALUES (NULL, 'superadmin', 'hgy@iworkshop.hu', '$2y$10$DcBf6FEO0nhVrJNm9JK8Mu1AhhvWijivS8Tg4khYs8n5WgCuAWUiy', '1', NULL, '2020-06-20 11:42:57', NULL);
        */
        $qry = "INSERT INTO `admins` (
                        `username`, 
                        `email`, 
                        `password`, 
                        `status`, 
                        `time_created`) 
                        VALUES (
                        '{$data['name']}', 
                        '{$data['email']}', 
                        '{$data['password']}', 
                        '{$data['status']}', 
                        '{$data['registrationDate']}')";//kérés összeállítása

        //kérés futtatása vagy állj
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //átirányítás a listázásra
        header('location:user-list.php');
        exit();
        //echo '<pre>' . var_export($data, true) . '</pre>';
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Regisztráció</title>
    <style>
        html, body {
            padding: 0;
            margin: 0;
        }

        form {
            padding: 15px;
        }

        label {
            display: block;
            margin: 15px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 11px;
        }
    </style>
</head>
<body>
<form method="post">
    <label>
        név<sup>*</sup>
        <input type="text"
               name="name"
               placeholder="Gipsz Jakab"
               value="<?php echo filter_input(INPUT_POST, 'name'); ?>"
        >
        <?php
        //hiba kiírása ha van
        if (isset($hiba['name'])) {
            echo $hiba['name'];
        }
        ?>
    </label>
    <label>
        email<sup>*</sup>
        <input type="text"
               name="email"
               placeholder="email@cim.hu"
               value="<?php echo filter_input(INPUT_POST, 'email'); ?>"
        >
        <?php
        //hiba kiírása ha van
        if (isset($hiba['email'])) {
            echo $hiba['email'];
        }
        ?>
    </label>
    <label>
        jelszó<sup>*</sup>
        <input type="password" name="pass" value="">
        <?php
        echo hibaKiir('pass');
        ?>
    </label>
    <label>
        jelszó újra<sup>*</sup>
        <input type="password" name="repass" value="">
        <?php
        echo hibaKiir('repass');
        ?>
    </label>
    <label>
        státusz
        <input type="checkbox" name="status" value="1" <?php echo filter_input(INPUT_POST,'status')?'checked':''; ?>

    </label>
    <p>
        A regisztrálok gomb megnyovásával igazolja hogy elolvasta és megértette az
        <a href="#lint-to-gdpr" target="_blank">adatkezelési
            tájokaztatóban</a> foglaltakat!
    </p>
    <button>Regisztrálok</button>
</form>
</body>
</html>
<?php
/**
 * Saját hiba kiíró eljárás
 * a $hiba változóban tárolt hibák kiírására
 * @param $inputName
 * @return bool
 */
function hibaKiir($inputName)
{
    global $hiba;//az eljárás idejére elérhetővé tesszük a hibatömböt

    if (isset($hiba[$inputName])) {
        return $hiba[$inputName];
    }
    return false;
}

