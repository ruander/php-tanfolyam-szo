<?php
//erőforrások
include_once("connect.php");//$link
//url adatok ha vannak (action,id)
$act = filter_input(INPUT_GET, "action") ?: false;//ha kapunk adatot action kulcsot akkor átvesszük, különben fals-ra állítjuk
$tid = filter_input(INPUT_GET,"id",FILTER_VALIDATE_INT);
//ha törölni kell törlünk
if($act == 'delete' && $tid > 0){//ha az action törlés és kapunk értelmes id-t, megpróbáljuk törölni
    mysqli_query($link, "DELETE FROM admins WHERE id = '$tid' LIMIT 1") or die(mysqli_error($link));
    //törlés után hogy ne maradjon bent a vezérlő url, listára irányítunk
    header('location:'.$_SERVER['PHP_SELF']);
    exit();
}
//adminok lekérése
$qry = "SELECT * FROM admins";
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
//adminok táblázat összeállítása
$table = '<a href="admin-create.php">Új felvitel</a>
               <br>
               <table border="1">
                <tr>
                    <th>id</th>
                    <th>username</th>
                    <th>email</th>
                    <th>státusz</th>
                    <th>műveletek</th>
                </tr>';
//sorok kialakítása
while (null !== $row = mysqli_fetch_assoc($result)) {
    $table .= "<tr>
                        <td>{$row['id']}</td>
                        <td>{$row['username']}</td>
                        <td>{$row['email']}</td>
                        <td>{$row['status']}</td>
                        <td><a href=\"admin-update.php?id={$row['id']}\">módosít</a> | <a href=\"?action=delete&amp;id={$row['id']}\">töröl</a></td>
                    </tr>";
}
$table .= '</table>';

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>felh. lista</title>
</head>
<body>
<h2>Adminok</h2>

<?php echo $table; ?>

</body>
</html>