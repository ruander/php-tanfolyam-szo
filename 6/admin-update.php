<?php
//erőforrások
include_once("connect.php");
$tid = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);//id url-ből
//ha van id akkor lekérjük az adatokat, ha nincs akkor vissza a listára
if ($tid > 0) {
    $qry = "SELECT * FROM admins WHERE id = $tid LIMIT 1 ";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);
    var_dump($row);
} else {
    //mivel ez update form ezért ha nincs id, akkor nincs itt dolgunk
    header('location:user-list.php');
    exit();
}
if (!empty($_POST)) {
    $hiba = [];//hibák tömbje
    //nev - nem lehet üres
    $name = trim(filter_input(INPUT_POST, 'name'));
    //szövegvégi spacek eltávolításával
    //$name = trim($name);//spacek eltávolítása a szoveg elejéről és végéről
    if ($name == '') {
        $hiba['name'] = '<span class="error">Kötelező kitölteni!</span>';
    }
    //email - legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Érvénytelen adat!</span>';
    }
    //jelszó min 6 karakter kell legyen
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');

    //updaten vagyunk, ha üres az 1. jelszó mező akkor kihagyjuk az ellenőrzést
    $passQry = '';//ha nem kell pass Update mert nem adtak jelszavakat
    if($pass !== '') {
        if (mb_strlen($pass, "utf-8") < 6) {
            $hiba['pass'] = '<span class="error">Érvénytelen adat (min 6 karakter) !</span>';
        } elseif ($pass != $repass) { //jelszó újra meg kell egyezzen a jelszóval
            $hiba['repass'] = '<span class="error">Érvénytelen adat (a jelszavak nem egyeztek) !</span>';
        } else {
            //$secret_key = 'S3cR3t_K3y!';
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            //password_hash visszafejtése: password_verify(jelszo,tárolt hash)
        }
        //password query kiegészítés
        $passQry = ", `password` = '$pass' ";//kell password update mert adtak jó jelszavakat
    }
    //status
    $status = filter_input(INPUT_POST,'status')?:0;

    if (empty($hiba)) {
        //adatok 'tisztázása'
        $data = [
            'name' => $name,
            'email' => $email,
            'status' => $status,
            'time_updated' => date('Y-m-d H:i:s')
        ];
        //admin update process
        /*
        UPDATE admins SET username =  'username', email =  'emailvcim.hu',... WHERE id = 123 LIMIT 1
        */
        $qry = "UPDATE admins SET 
                    username = '{$data["name"]}', 
                    email = '{$data["email"]}',
                    status = '{$data["status"]}',
                    time_updated = '{$data["time_updated"]}'
                    $passQry
                    WHERE id = $tid 
                    LIMIT 1";//kérés összeállítása

        //kérés futtatása vagy állj
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //menjünk vissza a listázásra
        header('location:user-list.php');exit();
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Regisztráció</title>
    <style>
        html, body {
            padding: 0;
            margin: 0;
        }

        form {
            padding: 15px;
        }

        label {
            display: block;
            margin: 15px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 11px;
        }
    </style>
</head>
<body>
<form method="post">
    <label>
        név<sup>*</sup>
        <input type="text"
               name="name"
               placeholder="Gipsz Jakab"
               value="<?php echo checkMyInput('name', $row['username']); ?>"
        >
        <?php
        //hiba kiírása ha van
        if (isset($hiba['name'])) {
            echo $hiba['name'];
        }
        ?>
    </label>
    <label>
        email<sup>*</sup>
        <input type="text"
               name="email"
               placeholder="email@cim.hu"
               value="<?php echo checkMyInput('email', $row['email']); ?>"
        >
        <?php
        //hiba kiírása ha van
        if (isset($hiba['email'])) {
            echo $hiba['email'];
        }
        ?>
    </label>
    <label>
        jelszó<sup>*</sup>
        <input type="password" name="pass" value="">
        <?php
        echo hibaKiir('pass');
        ?>
    </label>
    <label>
        jelszó újra<sup>*</sup>
        <input type="password" name="repass" value="">
        <?php
        echo hibaKiir('repass');
        ?>
    </label>
    <label>
        <input type="checkbox" name="status"
               value="1" <?php echo checkMyInput('status', $row['status'], 'checkbox'); ?> >
        státusz
    </label>
    <br>
    <button>Módosítok</button>
</form>
</body>
</html>
<?php
/**
 * Saját hiba kiíró eljárás
 * a $hiba változóban tárolt hibák kiírására
 * @param $inputName
 * @return bool
 */
function hibaKiir($inputName)
{
    global $hiba;//az eljárás idejére elérhetővé tesszük a hibatömböt

    if (isset($hiba[$inputName])) {
        return $hiba[$inputName];
    }
    return false;
}

/**
 * @param $fieldName
 * @param $rowData
 * @param string $type
 * @return mixed|string
 */

function checkMyInput($fieldName, $rowData, $type = 'text')
{
    switch ($type) {
        case 'checkbox':
            return checkBoxTest($fieldName, $rowData);
        default:
            return checkValue($fieldName, $rowData);
    }
}


/*Értékek visszaadása az űrlapok elemeihez*/
/**
 * @param $fieldName :string
 * @param $rowData :string
 * @return mixed|string
 */
function checkValue($fieldName, $rowData)
{
    $fieldData = filter_input(INPUT_POST, $fieldName);
    if ($fieldData !== null) {
        return $fieldData;//ha van post adat azt adjuk vissza ha nincs,
    } elseif ($rowData != '') { //de van db adat akkor azt adjuk vissza
        return $rowData;
    }
    return '';//sehol nincs semmi, azt adjuk vissza
}

//ez most a checkboxokhoz lesz:
function checkBoxTest($fieldName, $rowData)
{
    $fieldData = filter_input(INPUT_POST, $fieldName);

    if (empty($_POST) && $rowData == 0 || !empty($_POST) && $fieldData === NULL) { //ha üres a post és row 0 akkor nem kell checked || van post de nincs kipipálva tehát nincs a postban az adott elem
        return '';
    }

    return 'checked';
}

