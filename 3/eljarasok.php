<?php
$szoveg = 'Hello World!';

echo "<h1 class='mainTitle'>$szoveg</h1>";

$szoveg = 'Horváth György';

echo "<h1 class='mainTitle'>$szoveg</h1>";


myTitle();//eljárás hívása
//saját eljárás maintitle kiírására bármilyen szöveggel (:void)
/*function myTitle(){
    //programrész
    echo "<h1 class='mainTitle'>Eljarasból</h1>";
}*/
function myTitle($text = 'kéne szöveg paraméterbe...')
{//kötelező paraméter
    //programrész
    echo "<h1 class='mainTitle'>$text</h1>";
}

myTitle('ez egy teszt...');
echo myTitle2();//közvetlen kiírás
$test = myTitle2('woivghfn');//eredmény változóba és aztán kiírás
echo $test;
/**
 * nem void eljárás címek létrehozására
 * @param string $text
 * @return string
 */
function myTitle2($text = '...')
{
    $ret = "<h2 class='subTitle'>$text</h2>";//ezzel fogunk visszatérni
    return $ret;//visszatérünk 1 valamivel
}

/**
 * @param string $phrase
 * @return string (html tagekkel)
 * @todo saját eljárás idézetek kiírására:
 * halványszürke háttérrel, piros dőlt betűvel, mindig uj sorban az adott terület közepén
 * myPhraser eljárás
 */

echo myPhraser('Száguldó vonatról leszállni csak egyetlenegyszer lehetséges');
function myPhraser($phrase)
{
    $ret = '<em style="background: #efefef;display:block;text-align: center;clear: both;color:red;padding: 15px;">' . $phrase . '</em>';
    return $ret;
}

//kérjünk egy páros véletlenszámot
echo getEvenNumber(); //2 , 46 , 346
/**
 * @return int
 */
function getEvenNumber()
{
    $number = rand();
    //ha nem páros akkor addig generáljunk amig nem kapunk azt
    while ($number % 2 != 0) {
        $number = rand();
    }
    return $number;//térjünk vissza a biztosan páros számmal
}
//eljárás, ami létrehoz egy paraméterben megadott elemszámu listát item 1, item 2 ...., ha nincs megadva az elemszám paraméterben akkor az legyen 5
echo myList(10);
/**
 * @param int $items
 * @return string
 */
function myList($items = 5){
    $ret = '<ul>';
    //listaelemek
    for($i=1;$i<=$items;$i++){
        $ret .='<li>item '.$i.'</li>';
    }
    $ret .= '</ul>';
    //térjünk vissza a listával
    return $ret;
}
//hozzunk létre egy 20 elemű 400 és 10000 közé eső 10el osztható számokból álló tömböt
//[ 450, 980, 1180, 2710, ...]
var_dump( '<pre>',myTestArray() );//array
function myTestArray(){
    $ret = [];//itt lesznek az elemeink
    for($i=1;$i<=20;$i++){
        //ciklus az elemek generálására
        //a feladat megfogalmazásából látszik, hogy 10 el osztható kell legyen ezért egy trükköt alkalmazok
        $ret[] = rand(40,1000)*10;
    }
    //térjünk vissza a tömbbel
    return $ret;
}
/**
 * ha nincs return, akkor az eljár un :void
 * a másik ha van return
 *
 *
 * function eljarasNeve([param1,param2,...]){
 * //programrész
 * (return $valami)
 * }
 */
echo array_sum([28, 19, 20, 20, 29, 26, 25, 33, 30, 30, 25, 26, 22, 22, 28, 29, 27, 28, 27, 31, 28, 30, 22, 24, 23, 24, 25, 27, 25, 24])/30;