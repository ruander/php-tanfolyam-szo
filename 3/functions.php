<?php
//az 1 mappában létrehozott univerzális kiemeléses táblázat átültetése eljárásba
/**
 * Eljárás létrehozására minta eljárás
  <p>eljárás <b>bővebb</b> leírása</p>
 * @param int $rows
 * @param int $cols
 * @param mixed $kiemeltSor | bővebb leírás
  <p>bővebb leírás...</p>
 * @param mixed $kiemeltOszlop
 * @param array $kizartSorElemek
 * @return string
 */
function kiemelesesTabalazat($rows = 5,$cols = 5, $kiemeltSor = false,$kiemeltOszlop = false, $kizartSorElemek = []){
    $table = '<table border="1">';//table nyitás

//sorok
    for($sor=1;$sor<=$rows;$sor++){

        if(//kiemelt sor kell legyen?
            $kiemeltSor !== false //kell kiemelés
            AND //és
            ( $sor == 1 //első sorba mindenképp
                OR //vagy
                //nem az első sorban vagyunk
                $sor%$kiemeltSor == 0 //kell kiemelés
                AND //és
                $kiemeltSor > 1 //kell több kiemelés
            )
            AND !in_array($sor,$kizartSorElemek)
        ){
            $table .= '<tr class="kiemelt">';
        }else{
            $table .= '<tr>';
        }
        //oszlopok / belső (beágyazott) ciklus
        for($oszlop=1;$oszlop<=$cols;$oszlop++){
            //kiemelt oszlop-e? === !== == !=
            if(
                $kiemeltOszlop !== false //kell kiemelés
                AND //és
                ( $oszlop == 1 //első oszlopba mindenképp
                    OR //vagy
                    //nem az első oszlopban vagyunk
                    $oszlop%$kiemeltOszlop == 0 //kell kiemelés
                    AND //és
                    $kiemeltOszlop > 1 //kell több kiemelés
                )
            ){
                $table .= "<td class='kiemelt'>cella: $sor | $oszlop</td>";
            }else{
                $table .= "<td>cella: $sor | $oszlop</td>";
            }
        }
        $table .='</tr>';
    }
    $table .= '</table>';//táblázat zárása

    return $table;//visszatérünk a komplett táblázattal
}