<?php
//saját eljárás(ok) betöltése (mintha ide lenne gépelve)
//include "functions.php";//ha nincs notice és megy tovább
//include_once "functions.php";//csak ha nem volt még includeja a filenak
//require "functions.php";//ha nincs, die
require_once "functions.php";//csak ha nem volt még includeja a filenak
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>eljárások include bemutatása</title>
    <style>
        .kiemelt {
            background: burlywood;
            color: darkblue;
        }
    </style>
</head>
<body>
<?php
//táblázat készítése a saját eljárás hívásával
echo kiemelesesTabalazat(10,4, 5,false,[1]);
?>

</body>
</html>