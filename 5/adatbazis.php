<?php
require_once "connect.php";//db csatl.

//műveletek az adatbázisban
$qry = "SELECT * FROM employees";//kérés összeállítása
$results = mysqli_query($link,$qry) or die(mysqli_error($link));//kérés futtatása
$row = mysqli_fetch_row($results);//egy sor kibontása
echo '<pre>'.var_export($row,true).'</pre>';
$row = mysqli_fetch_assoc($results);//egy sor kibontása
echo '<pre>'.var_export($row,true).'</pre>';
$row = mysqli_fetch_array($results);//egy sor kibontása
echo '<pre>'.var_export($row,true).'</pre>';
$row = mysqli_fetch_object($results);//egy sor kibontása
echo '<pre>'.var_export($row,true).'</pre>';
//többi elem fetchelése 1 lépésben
$rows = mysqli_fetch_all($results,MYSQLI_ASSOC);
echo '<pre>'.var_export($rows,true).'</pre>';
//mi maradt?
$row = mysqli_fetch_row($results);//egy sor kibontása
echo '<pre>'.var_export($row,true).'</pre>';

//irodák lekérése és listába helyezése
$qry = "SELECT * FROM offices";
$results = mysqli_query($link,$qry) or die(mysqli_error($link));
//kibontás ciklusban
$lista = '<ul>';//lista nyitása
while($row = mysqli_fetch_assoc($results)){
    $lista .= '<li>'.$row['country'].' | '.$row['city'].' | tel: '.$row['phone'].'</li>';
    //echo '<pre>'.var_export($row,true).'</pre>';
}
$lista .='</ul>';
echo $lista;//lista kiírása
//gyakorlás
$time = microtime(true);

$qry = "SELECT productcode,productname,msrp FROM products";
$results = mysqli_query($link,$qry) or die(mysqli_error($link));
//kibontás ciklusban
$lista = '<ul>';//lista nyitása
while($row = mysqli_fetch_assoc($results)){
    $lista .= '<li>'.$row['productcode'].' - '.$row['productname'].' | ára: '.$row['msrp'].' USD</li>';
    //echo '<pre>'.var_export($row,true).'</pre>';
}
$lista .='</ul>';
echo $lista;//lista kiírása
echo $ellapsed_time = microtime(true) - $time;


$time = microtime(true);
$qry = "SELECT CONCAT(productcode,' - ',productname,' - Ára: ',msrp,' USD') FROM products";
$results = mysqli_query($link,$qry) or die(mysqli_error($link));
//kibontás ciklusban
$lista = '<ul>';//lista nyitása
while($row = mysqli_fetch_row($results)){
    $lista .= '<li>'.$row[0].'</li>';
    //echo '<pre>'.var_export($row,true).'</pre>';
}
$lista .='</ul>';
echo $lista;//lista kiírása
echo $ellapsed_time = microtime(true) - $time;