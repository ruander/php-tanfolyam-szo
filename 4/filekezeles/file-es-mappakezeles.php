<?php
//@todo: átnézni: https://www.php.net/manual/en/ref.dir.php
//@todo: átnézni: fopen,fread ,fwrite,fclose - https://www.php.net/manual/en/ref.filesystem.php

$dir = 'test/';
//létezik e a mappa?
if (!is_dir($dir)) {//ha nem létezik hozzuk létre
    mkdir($dir, 0755, true);//mappa létrehozása, rekurzív lehetőséggel //0644
    //echo 'nem létezik';
}
$fileName = 'myfile.data';
$targetPath = $dir . $fileName;//filenév és mappa
//file létének vizsgálata

$data = [
    'teszt' => 'Hello World!',
    12 => 'test content uj 2'
];
var_dump('<pre>',$data);
//filebaírás előtt primitív formára hozzuk, (string) tárolás idejére
$fileContent = json_encode($data);
//file_put_contents tömb esetén az értékeket tárolja egymás után
file_put_contents($targetPath, $fileContent);//fopen,fwrite,fclose


//file tartalmának visszaolvasása
$contentFromFile = file_get_contents($targetPath);//fopen,fread,fclose
echo $contentFromFile;//kiírjuk a kapott stringet
var_dump( json_decode($contentFromFile,true) );//eredetivel egyezik
//file törlése
//unlink($targetPath);

//töröljük az üresen!!!! maradt test mappát
//rmdir($dir);

//csv formátum
$fileNameCSV = 'data.csv';
//file megnyitása írásra
$fp=fopen($dir.$fileNameCSV,"w");
fputcsv($fp,$data);
fclose($fp);

//visszaolvasás
$fp=fopen($dir.$fileNameCSV,"r");
$dataFromCSV = fgetcsv($fp);
var_dump($dataFromCSV);