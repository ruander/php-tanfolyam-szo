<?php
if (!empty($_POST)) {
    $hiba = [];//hibák tömbje
    //nev - nem lehet üres
    $name = trim(filter_input(INPUT_POST, 'name'));
    //szövegvégi spacek eltávolításával
    //$name = trim($name);//spacek eltávolítása a szoveg elejéről és végéről
    if ($name == '') {
        $hiba['name'] = '<span class="error">Kötelező kitölteni!</span>';
    }
    //email - legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Érvénytelen adat!</span>';
    }
    //jelszó min 6 karakter kell legyen
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    if (mb_strlen($pass, "utf-8") < 6) {
        $hiba['pass'] = '<span class="error">Érvénytelen adat (min 6 karakter) !</span>';
    } elseif ($pass != $repass) { //jelszó újra meg kell egyezzen a jelszóval
        $hiba['repass'] = '<span class="error">Érvénytelen adat (a jelszavak nem egyeztek) !</span>';
    }else{
        //$secret_key = 'S3cR3t_K3y!';
        $pass = password_hash($pass,PASSWORD_BCRYPT);
        //password_hash visszafejtése: password_verify(jelszo,tárolt hash)
    }


    if (empty($hiba)) {
        //adatok 'tisztázása'
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $pass,
            'registrationDate' => date('Y-m-d H:i:s')
        ];
        //műveletek
        /*
         * @todo: tároljuk el a regisztrált felh adatait json formátumban egy fileban. a FILE NEVE LEGYEN: 'user-time().json -> user-1234242354.json,A mappa neve legyen: users/
       users/user-1591450158.json
        */
        echo '<pre>'.var_export($data,true).'</pre>';
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Regisztráció</title>
    <style>
        html, body {
            padding: 0;
            margin: 0;
        }

        form {
            padding: 15px;
        }

        label {
            display: block;
            margin: 15px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 11px;
        }
    </style>
</head>
<body>
<form method="post">
    <label>
        név<sup>*</sup>
        <input type="text"
               name="name"
               placeholder="Gipsz Jakab"
               value="<?php echo filter_input(INPUT_POST, 'name'); ?>"
        >
        <?php
        //hiba kiírása ha van
        if (isset($hiba['name'])) {
            echo $hiba['name'];
        }
        ?>
    </label>
    <label>
        email<sup>*</sup>
        <input type="text"
               name="email"
               placeholder="email@cim.hu"
               value="<?php echo filter_input(INPUT_POST, 'email'); ?>"
        >
        <?php
        //hiba kiírása ha van
        if (isset($hiba['email'])) {
            echo $hiba['email'];
        }
        ?>
    </label>
    <label>
        jelszó<sup>*</sup>
        <input type="password" name="pass" value="">
        <?php
        echo hibaKiir('pass');
        ?>
    </label>
    <label>
        jelszó újra<sup>*</sup>
        <input type="password" name="repass" value="">
        <?php
        echo hibaKiir('repass');
        ?>
    </label>
    <p>
        A regisztrálok gomb megnyovásával igazolja hogy elolvasta és megértette az
        <a href="#lint-to-gdpr" target="_blank">adatkezelési
            tájokaztatóban</a> foglaltakat!
    </p>
    <button>Regisztrálok</button>
</form>
</body>
</html>
<?php
/**
 * Saját hiba kiíró eljárás
 * a $hiba változóban tárolt hibák kiírására
 * @param $inputName
 * @return bool
 */
function hibaKiir($inputName)
{
    global $hiba;//az eljárás idejére elérhetővé tesszük a hibatömböt

    if (isset($hiba[$inputName])) {
        return $hiba[$inputName];
    }
    return false;
}

