<?php
echo '<pre>';
//var_dump($_SERVER);//szuperglobális szerver info tömb
    var_dump($_GET);//szuperglobális GET tömb
echo '</pre>';
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlapok</title>
    <style>
        html,body {
            padding: 0;
            margin: 0;
        }
        form {
            padding: 15px;
        }
        label {
            display:block;
            margin: 15px 0;
        }
    </style>
</head>
<body>
<section>
<h1>A GET tipusu űrlap</h1>
<form method="get" action="feldolgoz.php?valami2=teszt2">
    <label for="email">Email<sup>*</sup></label>
    <input name="email" type="text" placeholder="email@cim.hu" id="email">
    <label>
        Adj meg egy számot<sup>*</sup> <input type="text" name="szam" value="345" placeholder="5">
    </label>
    <!--<input name="submit" type="submit" value="Gomb felirata">-->
    <button>Mehet</button>
</form>
</section>
<section>
    <h1>A POST tipusu űrlap</h1>
    <form method="post" action="feldolgoz.php?valami=teszt&amp;helo=world">
        <label>
            Név<sup>*</sup> <input type="text" name="nev" placeholder="John Doe" value="">
        </label>
        <button>Mehet POST</button>
    </form>
</section>

</body>
</html>