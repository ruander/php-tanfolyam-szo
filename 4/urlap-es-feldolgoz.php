<?php
//adatok feldolgozása, ha azok léteznek (nem üres a POST)
$output = '';//ide gyűjtjük a doctype előtt keletkező kiírandó adatokat

if (!empty($_POST)) {
    $hiba = [];//üres hiba tömb a hibáknak
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //echo $_POST['szam'];//ezt így KERÜLD!!!!!
    $szam = filter_input(INPUT_POST, 'szam', FILTER_VALIDATE_INT);//ellenőrzés egész szám szűrővel
    //var_dump($szam);
    if (!$szam) {/*ez a 0 értékre is hibát ad, ha kell a 0 is mint nem hibás érték kkor:
    if ($szam === false OR $szam === null) {*/
        //die('hiba');
        $hiba['szam'] = '<span class="error">Hibás adat!</span>';
    }

    var_dump($hiba);
    if (empty($hiba)) {//ha üres maradt akkor jok az adatok
        //művelet a (jó) adatokkal
        //páros vagy páratlan a szám amit beírtak (ezt írjuk majd ki a példában)
        if ($szam % 2 == 0) {
            $output .= 'Páros';//kiírando output változóba fűzése
        } else {
            $output .= 'Páratlan';
        }

    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlap és hibakezelés</title>
    <style>
        html, body {
            padding: 0;
            margin: 0;
        }

        form {
            padding: 15px;
        }

        label {
            display: block;
            margin: 15px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 11px;
        }
    </style>
</head>
<body>
<div class="output">
    <?php echo $output;//kiírandók a doctype előttről ?>
</div>
<form method="post">
    <label>
        Adj meg egy számot<sup>*</sup>
        <input type="text"
               name="szam"
               value="<?php echo filter_input(INPUT_POST, 'szam'); ?>"
               placeholder="5"
               autocomplete="off"
        > <?php
        //kiírjuk a hibát, ha van
        if (isset($hiba['szam'])) {
            echo $hiba['szam'];
        }
        ?>
    </label>
    <button>mehet</button>
</form>
</body>
</html>