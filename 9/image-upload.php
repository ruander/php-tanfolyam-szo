<?php
//Képkezelés | https://www.php.net/manual/en/book.image.php
/*
 Az eredeti méretből kiindulva legyen , 150x150px thumbnail, egy max 650x650px dobozba helyezhető kép
1200x450px slider vágott változat, eredeti (max full hd)
 */
$allowedImageTypes = [
    'image/jpeg',
    'image/jpg',
    //'image/png'
];
if (!empty($_POST)) {
    //var_dump($_POST, $_FILES);
    if ($_FILES['image_to_upload']['error'] == 0) {//ha volt feltöltött file
        $imageFile = $_FILES['image_to_upload']['tmp_name'];
        $imageOutputName = ekezettelenitFile($_FILES['image_to_upload']['name']);
//kép file infoi, ha nem kép akkor false
        $imageInfo = getimagesize($imageFile);
//echo '<pre>' . var_export($imageInfo, true) . '</pre>';
//ha kép - méretarányos kicsinyítése
        if ($imageInfo !== false) {
            //itt lehet ellenőrizni hogy a mime-type egyezik e a feltölthető tipusokkal
            if(!in_array($imageInfo['mime'],$allowedImageTypes)){
              echo  $hiba['image_to_upload'] = 'Nem engedélyezett filetipus!';
            }
            $width = $imageInfo[0];//szélesség
            $height = $imageInfo[1];//magasság
//méretarányos kicsinytés lépései
            //álló v fekvő?
            $ratio = $width / $height;
            if ($ratio > 1) {
                //fekvő
                $targetWidth = 650;
                $targetHeight = round($targetWidth / $ratio);
            } else {
                //négyzet v álló
                $targetHeight = 650;
                $targetWidth = round($targetHeight * $ratio);
            }

            $canvas = imagecreatetruecolor($targetWidth, $targetHeight);//vászon, ide kerül majd a kép
            $image = imagecreatefromjpeg($imageFile);//forráskép memóriában

            imagecopyresampled($canvas, $image, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);

            //header("Content-Type: image/jpeg");//ez a file látszódjon képnek
            //mappa legyen most: kep-650/eredetinev
            $dir = "kep-650/";
            if (!is_dir($dir)) {
                mkdir($dir, 0755, true);
            }
            imagejpeg($canvas, $dir . $imageOutputName, 80);
        }
//négyzet thumbnail készítése (crop)

        if ($imageInfo !== false) {
            $width = $imageInfo[0];//szélesség
            $height = $imageInfo[1];//magasság
//CROP lépései
            //álló v fekvő?
            $ratio = $width / $height;
            $targetWidth = $targetHeight = 150;
            if ($ratio > 1) {
                //fekvő
                $tempHeight = $targetHeight;
                $tempWidth = round($ratio * $targetHeight);
                $offset_x = round(($tempWidth - $targetWidth) / 2);
                $offset_y = 0;
            } else {
                //négyzet v álló
                $tempWidth = $targetWidth;
                $tempHeight = round($targetWidth / $ratio);
                $offset_y = round(($tempHeight - $targetHeight) / 2);
                $offset_x = 0;
            }

            $canvas = imagecreatetruecolor($targetWidth, $targetHeight);//vászon, ide kerül majd a kép
            $image = imagecreatefromjpeg($imageFile);//forráskép memóriában

            imagecopyresampled($canvas, $image, -$offset_x, -$offset_y, 0, 0, $tempWidth, $tempHeight, $width, $height);

            //header("Content-Type: image/jpeg");//ez a file látszódjon képnek
            //mappa legyen most: kep-650/eredetinev
            $dir = "thumbs/";
            if (!is_dir($dir)) {
                mkdir($dir, 0755, true);
            }
            imagejpeg($canvas, $dir . $imageOutputName, 80);
        }
    }
}
//


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Képkezelés</title>
</head>
<body>
<form method="post" enctype="multipart/form-data">
    <label>Kép feltöltése: <input type="file" name="image_to_upload"></label>
    <button name="submit">Gyííí</button>
</form>
</body>
</html><?php


/*
 * string kezelő eljárások
 */
//ékezettelenítő eljárás
function ekezettelenitFile($str)
{
    $bad = array('á', 'ä', 'é', 'í', 'ó', 'ö', 'ő', 'ú', 'ü', 'ű', 'Á', 'Ä', 'É', 'Í', 'Ó', 'Ö', 'Ő', 'Ú', 'Ü', 'Ű', ' ');
    $good = array('a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', 'a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', '-');
    $str = mb_strtolower($str, "utf8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str, '-');//végződő - eltávolítása
    return $str;
}
