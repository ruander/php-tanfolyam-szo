<?php
//public domain, ahol az oldal publikus felülete van
$public_domain = 'http://public.phptanfolyam.szombat';
//admin domain, ahol az oldal adminisztrációs felülete van
$admin_domain = 'http://admin.phptanfolyam.szombat';
//titkosító kulcs
$secret_key = 'S3cR3t_K3Y!';
//modulok mappája
$moduleDir = 'modules/';
$moduleExt = '.php';
//admin menü
$adminMenu = [
    0 => [
        'title' => 'Vezérlőpult',
        'icon' => 'fas fa-tachometer-alt',
        'moduleName' => 'dashboard'
    ],
    1 => [
        'title' => 'Cikkek',
        'icon' => 'fa fa-address-card',
        'moduleName' => 'articles'
    ],
    200 => [
        'title' => 'Adminisztrátorok',
        'icon' => 'fa fa-user',
        'moduleName' => 'admins'
    ],
    //...
];