<?php
/**
 * Beléptetés
 * @return bool
 */
function login()
{
    global $link, $secret_key;//globálissá teszük az eljárás idejére, hogy 'lássa'

    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $password = filter_input(INPUT_POST, 'password');
    if ($email) {
//lekérjük a hozzá tartozó jelszót
        $qry = "SELECT id,password,username FROM admins WHERE email = '$email' AND status = '1' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $admin = mysqli_fetch_assoc($result);
//var_dump($admin);
//jelszó helyesség ellenőrzése
        $pCheck = password_verify($password, $admin['password']);
        if ($pCheck) {
            $userData['id'] = $admin['id'];
            $userData['username'] = $admin['username'];
            $userData['email'] = $email;
            //var_dump($userData);
            //tárolás mf-ban
            $_SESSION['userdata'] = $userData;
            $_SESSION['id'] = session_id();
            //mf adattárolás db ben a későbbi authentikációhoz
            //sid,stime,spass
            $sid = $_SESSION['id'];
            $stime = time();//ekkor történt a belépés
            $spass = md5($admin['id'] . $sid . $secret_key);
            //biztos ami biztos töröljük az erre a mf ra vonatkozó sort
            mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
            //belépés tárolása db ben
            $qry = "INSERT INTO sessions(sid,spass,stime)
                        VALUES('$sid','$spass','$stime')";
            mysqli_query($link, $qry) or die(mysqli_error($link));
            return true;
        }
    }
    //vmi nem sikerült, password, session stb...
    return false;
}

/**
 * érvényes belépés ellenőrzése mf és db alapján
 * @return bool
 */
function auth()
{
    global $link, $secret_key;

//authentikáció azaz volt-e érvényes belépés, munkafolyamat alapokon
    $sid = session_id();//aktuális mf azonosító
    $now = time();//most ennyi az 'idő'  -timestamp
//lejárt munkafolyamatok törlése
    $expired = $now - 900;//most 15p
    mysqli_query($link, "DELETE FROM sessions WHERE stime < $expired") or die(mysqli_error($link));
//van e bejegyzés a mf azonosítóra a db-ban
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
//gyártsuk le újra a mostani adatokból is az spasst
    $spass = md5($_SESSION['userdata']['id'] . $sid . $secret_key);
    if (!empty($row) && $spass === $row[0]) {
        //minden valós authentikáció siker esetén frissítjük az stime elemt....
        mysqli_query($link,"UPDATE sessions SET stime = $now ") or die(mysqli_error($link));
        return true;//stimmel, érvényes a bejelentkezés
    }
    return false;
}

/**
 * Kiléptetés
 * :void
 */
function logout(){
    global $link;
    mysqli_query($link, "DELETE FROM sessions WHERE sid = '{$_SESSION['id']}' LIMIT 1 ") or die(mysqli_error($link));
    $_SESSION = [];
    session_destroy();
}

/**
 * Saját hiba kiíró eljárás
 * a $hiba változóban tárolt hibák kiírására
 * @param $inputName
 * @return bool
 */
function hibaKiir($inputName)
{
    global $hiba;//az eljárás idejére elérhetővé tesszük a hibatömböt

    if (isset($hiba[$inputName])) {
        return $hiba[$inputName];
    }
    return false;
}

/**
 * @param $fieldName
 * @param $rowData
 * @param string $type
 * @return mixed|string
 */
function checkMyInput($fieldName, $rowData, $type = 'text')
{
    switch ($type) {
        case 'checkbox':
            return checkBoxTest($fieldName, $rowData);
        default:
            return checkValue($fieldName, $rowData);
    }
}

/*Értékek visszaadása az űrlapok elemeihez*/
/**
 * @param $fieldName :string
 * @param $rowData :string
 * @return mixed|string
 */
function checkValue($fieldName, $rowData)
{
    $fieldData = filter_input(INPUT_POST, $fieldName);
    if ($fieldData !== null) {
        return $fieldData;//ha van post adat azt adjuk vissza ha nincs,
    } elseif ($rowData != '') { //de van db adat akkor azt adjuk vissza
        return $rowData;
    }
    return '';//sehol nincs semmi, azt adjuk vissza
}

//ez most a checkboxokhoz lesz:
function checkBoxTest($fieldName, $rowData)
{
    $fieldData = filter_input(INPUT_POST, $fieldName);
    //var_dump($fieldName,$rowData);
    if (empty($_POST) && $rowData == 0 || !empty($_POST) && $fieldData === NULL) { //ha üres a post és row 0 akkor nem kell checked || van post de nincs kipipálva tehát nincs a postban az adott elem
        return '';
    }

    return 'checked';
}