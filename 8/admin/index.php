<?php
require("../config/connect.php");//adatbázis csatlakozás
require("../config/settings.php");//környezeti változók a rendszerben
require("../config/functions.php");//saját eljárások
//url paraméter kinyerése
$o = filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) ?: 0;//vagy kapunk vagy 0 azaz dashboard jelenleg
$baseUrl = 'index.php?p=' . $o;//moduloknak illetve linkeknek az aktuális modul idt tartalmazo alap url
session_start();//mf indítása
//var_dump($_SESSION);
//kiléptetünk ha kell
if (filter_input(INPUT_GET, 'logout')) {
    logout();
}
if (!auth()) {
    header('location:login.php');
    exit();//ha nincs érvényes belépés akkor irány a login
}
//innentől mehet a logika ami belépés után kell
//betöltendő modul neve
$moduleName = $adminMenu[$o]['moduleName'];
$moduleFile = $moduleDir . $moduleName . $moduleExt;
$output = '';//ha nincs semmilyen output vmiért
$styles = "";//itt lesznek a module stílusok
//létezik e az adott modul
if (file_exists($moduleFile)) {
    include $moduleFile;//ki van alakítva egy output változó a kiírandó elemeknek
} else {
    $output = "Nincs ilyen modul: $moduleFile";
}

//$adminBar = '<div>Üdvözöllek kedves ' . $_SESSION['userdata']['username'] . '! | <a href="?logout=true">kilépés</a></div>';
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin felület</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <?php
    echo $styles;//vagy üres string, vagy kapunk a modulból
    ?>
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <!--topBar-->
    <?php include "includes/topmenu.inc"; ?>
    <!--/topBar-->
    <!-- Main Sidebar Container -->
    <?php include "includes/main-sidebar.inc"; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Blank Page</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Blank Page</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><?php echo $adminMenu[$o]['title'];  ?></h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
                                title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <?php //output kiírása
                    echo $output; ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    Footer
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2020 <a href="https://Ruander">Ruander Oktatóközpont</a>.</strong> PHP programozó
        tanfolyam.
    </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>
</body>
</html>
