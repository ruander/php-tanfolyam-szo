<?php
//admins CRUD file
if(!$link){//önálló filemegnyitás védelem, plha nincs db link akkor vissza az admin indexre, ahol ha nincs joga megy a loginra
    header('location:../index.php');exit();
}
//@todo: update ágon módosításkor a név eltünik -hf: javítani
//@todo: create ág, se név se jelszó -hf:javítani
//@todo: tervezni egy articles táblát amibe cikkeket lehet tárolni

//url parméterek kinyerése | erőforrások
$tid = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);//id url-ből
$act = filter_input(INPUT_GET, "action") ?: 'list';//művelet
$output = '';//ide gyűjtjük a kiírandó elemeket
//hibakezelés az űrlaphoz
if (!empty($_POST)) {
    $hiba = [];//hibák tömbje
    //nev - nem lehet üres
    $name = trim(filter_input(INPUT_POST, 'name'));
    //szövegvégi spacek eltávolításával
    //$name = trim($name);//spacek eltávolítása a szoveg elejéről és végéről
    if ($name == '') {
        $hiba['name'] = '<span class="error">Kötelező kitölteni!</span>';
    }
    //email - legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Érvénytelen adat!</span>';
    } else {
        //foglalt-e már a db-ben
        $qry = "SELECT id FROM admins WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if (!empty($row) && $row[0] != $tid) {//ha találtunk email cimet és nem az aktuális módosítandó elemé
            $hiba['email'] = '<span class="error">Már regisztrált email cím!</span>';
        }
    }
    //jelszó min 6 karakter kell legyen
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    //1. ha uj felvitel
    //2. ha update és az 1. mezőben van legalább 1 karakter
    $passQry = '';//ha nem kell pass Update mert nem adtak jelszavakat
    if ($pass !== '' or $act == 'create') {
        if (mb_strlen($pass, "utf-8") < 6) {
            $hiba['pass'] = '<span class="error">Érvénytelen adat (min 6 karakter) !</span>';
        } elseif ($pass != $repass) { //jelszó újra meg kell egyezzen a jelszóval
            $hiba['repass'] = '<span class="error">Érvénytelen adat (a jelszavak nem egyeztek) !</span>';
        } else {
            //$secret_key = 'S3cR3t_K3y!';
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            //password_hash visszafejtése: password_verify(jelszo,tárolt hash)
        }
        //password query kiegészítés a későbbi update querynek
        $passQry = ", `password` = '$pass' ";//kell password update mert adtak jó jelszavakat
    }
    //státusz
    $status = filter_input(INPUT_POST, 'status') ?: 0;

    if (empty($hiba)) {
        //die('nincs hiba');
        //adatok 'tisztázása'
        $data = [
            'username' => $name,
            'email' => $email,
            //'password' => $pass,
            'status' => $status,
            //'registrationDate' => date('Y-m-d H:i:s')
        ];
        if ($act == 'create') {
            //adattömb create ág kiegészítései
            $data['password'] = $pass;
            $data['registrationDate'] = date('Y-m-d H:i:s');
            //new admin create process
            $qry = "INSERT INTO `admins` (
                        `username`, 
                        `email`, 
                        `password`, 
                        `status`, 
                        `time_created`) 
                        VALUES (
                        '{$data['username']}', 
                        '{$data['email']}', 
                        '{$data['password']}', 
                        '{$data['status']}', 
                        '{$data['registrationDate']}')";//kérés összeállítása
        }else{
            //admin update process
            //data kiegészítés update esetére
            $data['time_updated'] = date('Y-m-d H:i:s');
            //update query
            $qry = "UPDATE admins SET 
                    username = '{$data["username"]}', 
                    email = '{$data["email"]}',
                    status = '{$data["status"]}',
                    time_updated = '{$data["time_updated"]}'
                    $passQry
                    WHERE id = $tid 
                    LIMIT 1";
        }
        //kérés futtatása vagy állj
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //átirányítás a listázásra
        header('location:'.$baseUrl);
        exit();
        //echo '<pre>' . var_export($data, true) . '</pre>';
    }
}


//működés leválasztása
switch ($act) {
    case 'delete':
        //ha törölni kell törlünk
        if ($act == 'delete' && $tid > 0) {//ha az action törlés és kapunk értelmes id-t, megpróbáljuk törölni
            mysqli_query($link, "DELETE FROM admins WHERE id = '$tid' LIMIT 1") or die(mysqli_error($link));
            //törlés után hogy ne maradjon bent a vezérlő url, listára irányítunk
            header('location:' . $baseUrl);
            exit();
        }
        break;

    case 'update':
        //echo "Módosítok: $tid";
        if ($tid > 0) {
            $qry = "SELECT * FROM admins WHERE id = $tid LIMIT 1 ";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);
            //var_dump($row);
        }
    //break;

    case 'create':
        if (!isset($row)) {//ha új felvitel van akkor nem lesz $row
            $row = [
                'username' => '',
                'email' => '',
                'status' => ''
            ];
        }
        //name
        $form = '<form method="post" class="col-md-6">
    <div class="form-group"><label>
        név<sup>*</sup></label>
        <input class="form-control" type="text"
               name="name"
               placeholder="Gipsz Jakab"
               value="' . checkMyInput('name', $row['username']) . '" >';

        //hiba kiírása ha van
        if (isset($hiba['name'])) {
            $form .= $hiba['name'];
        }

        $form .= '</div>';
        //email
        $form .= '<div class="form-group"><label>
        email<sup>*</sup></label>
        <input class="form-control" type="text"
               name="email"
               placeholder="email@cim.hu"
               value="' . checkMyInput('email', $row['email']) . '" >';
        //hiba kiírása ha van
        if (isset($hiba['email'])) {
            $form .= $hiba['email'];
        }

        $form .= '</div>';
        //password 1
        $form .= '<div class="form-group"><label>
        jelszó<sup>*</sup></label>
        <input class="form-control" type="password" name="pass" value="">' . hibaKiir('pass') . '</div>';
//password 2
        $form .= '<div class="form-group"><label>
        jelszó újra<sup>*</sup></label>
        <input class="form-control" type="password" name="repass" value="">' . hibaKiir('repass') . '</div>';
        $form .= '
        <div class="form-check">
                          <input name="status" class="form-check-input" type="checkbox" value="1" ' . checkMyInput('status', $row['status'], 'checkbox') . '>
                          <label class="form-check-label">Státusz</label>
                        </div>';
        $form .= '<p>
        A regisztrálok gomb megnyovásával igazolja hogy elolvasta és megértette az
        <a href="#lint-to-gdpr" target="_blank">adatkezelési
            tájokaztatóban</a> foglaltakat!
    </p>
    <button class="btn btn-primary">Mehet</button>
</form>';
        $output = $form;
        break;

    default:
        //adminok lekérése
        $qry = "SELECT * FROM admins";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
//adminok táblázat összeállítása
        $table = '<div class="card"><a class="btn btn-success block m15" href="'.$baseUrl.'&amp;action=create">Új felvitel</a></div>
               <table class="table table-striped">
                <tr>
                    <th>id</th>
                    <th>username</th>
                    <th>email</th>
                    <th>státusz</th>
                    <th>műveletek</th>
                </tr>';
//sorok kialakítása
        while (null !== $row = mysqli_fetch_assoc($result)) {
            $table .= "<tr>
                        <td>{$row['id']}</td>
                        <td>{$row['username']}</td>
                        <td>{$row['email']}</td>
                        <td>{$row['status']}</td>
                        <td><a href=\"$baseUrl&amp;action=update&amp;id={$row['id']}\"><i class='fas fa-pencil-alt danger'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"$baseUrl&amp;action=delete&amp;id={$row['id']}\"><i class='fa fa-trash'></i></a></td>
                    </tr>";
        }
        $table .= '</table>';
        $output = $table;
        break;
}
//kimenet kiírása
//echo $output;//modulként ezt az index végzi
$styles = '<style>
        html, body {
            padding: 0;
            margin: 0;
        }

        form {
            padding: 15px;
        }

        label {
            display: block;
            margin: 15px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 11px;
        }
    </style>';
//echo $styles;
