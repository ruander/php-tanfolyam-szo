<?php
require("../config/connect.php");//adatbázis csatlakozás
require("../config/settings.php");//környezeti változók a rendszerben
require("../config/functions.php");//saját eljárások

session_start();//mf indítása
/*var_dump($_SESSION, session_id());//mf szuperglobális tömb és mf azonosító
$_SESSION['test'] = 'test content';
var_dump($_SESSION);*/
//session_destroy();//mf megsemmisítése/roncsolása
//ha van érvényes auth, akkor nincs dolga itt a felhasználónak
if (isset($_SESSION['userdata']) && auth()) {
    header("location:$admin_domain");
    exit();
}
$msg = '';//ha kiírunk vmit azt ide gyűjtjük
if (!empty($_POST)) {
    if (login() === true) {
        header("Location:index.php");
        exit();
    }

    //ha még ez a file fut, akkor vmi nem volt jó...
    $msg = '<span class="error">Nem megfelelő email/jelszó páros!</span>';
}


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adminisztráció - belépés</title>
</head>
<body>
<form method="post">
    <div>
        <?php echo $msg; ?>
    </div>
    <label>
        email <input type="text" name="email" id="email" placeholder="email@cim.hu"
                     value="<?php echo filter_input(INPUT_POST, 'email'); ?>">
    </label>
    <label>
        jelszó <input type="password" name="password" id="password" value="">
    </label>
    <button>Belépés</button>
</form>
</body>
</html>