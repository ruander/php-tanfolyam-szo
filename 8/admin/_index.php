<?php
require("../config/connect.php");//adatbázis csatlakozás
require("../config/settings.php");//környezeti változók a rendszerben
require("../config/functions.php");//saját eljárások
//url paraméter kinyerése
$o = filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) ?: 0;//vagy kapunk vagy 0 azaz dashboard jelenleg
$baseUrl = 'index.php?p='.$o;//moduloknak illetve linkeknek az aktuális modul idt tartalmazo alap url
session_start();//mf indítása
//var_dump($_SESSION);
//kiléptetünk ha kell
if (filter_input(INPUT_GET, 'logout')) {
    logout();
}
if (!auth()) {
    header('location:login.php');
    exit();//ha nincs érvényes belépés akkor irány a login
}
//innentől mehet a logika ami belépés után kell
//betöltendő modul neve
$moduleName = $adminMenu[$o]['moduleName'];
$moduleFile = $moduleDir . $moduleName . $moduleExt;
$output = '';//ha nincs semmilyen output vmiért
        $styles = "";//itt lesznek a module stílusok
//létezik e az adott modul
if (file_exists($moduleFile)) {
    include $moduleFile;//ki van alakítva egy output változó a kiírandó elemeknek
}else{
    $output = "Nincs ilyen modul: $moduleFile";
}

$adminBar = '<div>Üdvözöllek kedves ' . $_SESSION['userdata']['username'] . '! | <a href="?logout=true">kilépés</a></div>';
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin felület - </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php
    echo $styles;//vagy üres string, vagy kapunk a modulból
    ?>
</head>
<body>
<?php


echo $adminBar;

//menü felépítése
$menu = '<ul>';//lista
foreach ($adminMenu as $menuId => $menuItem) {
    $menu .= '<li><a href="?p=' . $menuId . '"><i class="' . $menuItem['icon'] . '"></i> ' . $menuItem['title'] . '</a></li>';
}
$menu .= '</ul>';
//menu kiírása
echo $menu;

//output kiírása
echo $output;
?>
</body>
</html>
