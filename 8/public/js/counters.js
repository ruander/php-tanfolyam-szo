//számlálók
/*
minta html: <div class="counter" data-target-number="x"><span>7</span> +</div>
additional: duration: data-duration="100" | ms
 */
let counters = document.getElementsByClassName('counter');//az összes elem
let timers = [],
    targets=[],
    targetNumbers = [],
    counterElemek = [],
    durations = [];
for(let c=0;c<counters.length;c++){
    targetNumbers[c] = counters[c].getAttribute("data-target-number");//aktuális célérték ameddig futnia kell
    counterElemek[c] = counters[c].querySelector('span');//ebbe az elembe írjuk az aktuális futó értékállapotot
    //console.log(targetNumbers);

    durations[c]= counters[c].getAttribute("data-duration");//elem időzítése
    targets[c]=0;//a futó értékek halmaza
    timers[c] = setInterval(function () {//időzítések is halmazba kerülnek
        counterElemek[c].innerHTML = targets[c]++;//i = i + 1 | az aktuális elem spanje vegye fel az aktuális növekvő értéket
        if (targets[c] >= targetNumbers[c]) {//elemenkét kell figyelni hogy elértük e a maximumot
            //ha meghaladtuk a kívánt értéket állítsuk le az időzítést
            clearInterval(timers[c]);
            //és gondoskodjunk arról, hogy biztosan a helyes érték legyen a mezőben
            counterElemek[c].innerHTML = targetNumbers[c];//aktuális szmláló időzítésének leállítása
        }
    }, durations[c]);//időzítések alkalmazása
}